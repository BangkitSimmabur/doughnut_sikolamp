-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2019 at 09:59 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sikolamp`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_lampu`
--

CREATE TABLE IF NOT EXISTS `t_lampu` (
  `id_lampu` int(10) NOT NULL,
  `status_lampu` int(1) NOT NULL,
  `status_fitur` int(1) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_lampu`
--

INSERT INTO `t_lampu` (`id_lampu`, `status_lampu`, `status_fitur`, `username`) VALUES
(2, 0, 0, 'jefrimanao'),
(4, 0, 1, 'offler');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `hak_akses` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`username`, `password`, `hak_akses`) VALUES
('agustian', 'poltek22', 1),
('jefrimanao', 'batam123', 1),
('offler', '123456', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_lampu`
--
ALTER TABLE `t_lampu`
  ADD PRIMARY KEY (`id_lampu`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_lampu`
--
ALTER TABLE `t_lampu`
  MODIFY `id_lampu` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_lampu`
--
ALTER TABLE `t_lampu`
  ADD CONSTRAINT `t_lampu_ibfk_1` FOREIGN KEY (`username`) REFERENCES `t_user` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
