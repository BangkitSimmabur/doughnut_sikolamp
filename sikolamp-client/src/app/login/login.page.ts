import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SikolampService } from '../sikolamp.service';
import { Response } from '@angular/http';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { PelangganPage } from '../pelanggan/pelanggan.page';
import { AdminPage } from '../admin/admin.page';
const KEY_DATA_LOCAL = "dataLocal";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  admin: any;
  admin1= {
    username: '',
    password: ''
  };
  constructor(
    public router:Router,
    public sikolamp: SikolampService,
    public storage: Storage,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    this.storage.set('name', 'mn');
    this.storage.get('name').then((val) => {
      console.log('Your Name Is ', val);
    })
  }

  async login(){
    const modal = await this.modalController.create({
      component: PelangganPage
    });
    modal.onDidDismiss().then(() => {});
    return await modal.present();
  }

  async login2(){
    const modal = await this.modalController.create({
      component: AdminPage
    });
    modal.onDidDismiss().then(() => {});
    return await modal.present();
  }

  async loginbeneran(){
    this.sikolamp.getadmin(this.admin1.username).subscribe((response: Response) => {
      if(response){
        this.admin = response.json();
        console.log(this.admin);
        if (this.admin[0].username == this.admin1.username){
          if (this.admin[0].password == this.admin1.password){
            if (this.admin[0].hak_akses == 1){
              this.login();
              localStorage.setItem('local_storage', this.admin[0].username);
              var wow = localStorage.getItem('local_storage');
            } else {
              this.login2();
            }
          }
        }
      }
    })
  }
}
