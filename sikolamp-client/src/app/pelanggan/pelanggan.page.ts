import { Component, OnInit } from '@angular/core';
import { SikolampService } from '../sikolamp.service';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

@Component({
  selector: 'app-pelanggan',
  templateUrl: './pelanggan.page.html',
  styleUrls: ['./pelanggan.page.scss'],
})
export class PelangganPage implements OnInit {
  
  listlampu: any ;
  constructor(
    public sikolampService: SikolampService,
    public modalController: ModalController,
    public router: Router,
    public route: ActivatedRoute,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    var username = localStorage.getItem('local_storage');
    console.log(username);
    this.sikolampService.LoadLampu(username).subscribe((response: Response)=>{
      let data = response.json();
      this.listlampu = data;
    });
    console.log(this.listlampu);
  }
  async presentActionSheetLampu() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Lampu',
      buttons: [{
        text: 'On',
        icon: 'contact',
        handler: () => {
          console.log('User Detail clicked');
        }
      }, {
        text: 'Off',
        icon: 'Create',
        handler: () => {
          console.log('Edit User clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  async presentActionSheetFitur() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Fitur',
      buttons: [{
        text: 'On',
        icon: 'contact',
        handler: () => {
          console.log('User Detail clicked');
        }
      }, {
        text: 'Off',
        icon: 'Create',
        handler: () => {
          console.log('Edit User clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  on(){
    
  }
  logout(){
    console.log("Berhasil Logout");
    this.modalController.dismiss();
  }
}
