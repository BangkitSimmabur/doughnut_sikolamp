-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 17, 2019 at 01:24 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sikolamp`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_lampu`
--

CREATE TABLE `t_lampu` (
  `id_lampu` int(10) NOT NULL,
  `status_lampu` int(1) NOT NULL,
  `status_fitur` int(1) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_lampu`
--

INSERT INTO `t_lampu` (`id_lampu`, `status_lampu`, `status_fitur`, `keterangan`, `username`) VALUES
(1, 1, 1, 'kamar', 'offler'),
(2, 1, 1, 'asdasd', 'offler');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `hak_akses` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`username`, `password`, `hak_akses`) VALUES
('admin', 'admin', 0),
('offler', '123123', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_lampu`
--
ALTER TABLE `t_lampu`
  ADD PRIMARY KEY (`id_lampu`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_lampu`
--
ALTER TABLE `t_lampu`
  MODIFY `id_lampu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_lampu`
--
ALTER TABLE `t_lampu`
  ADD CONSTRAINT `t_lampu_ibfk_1` FOREIGN KEY (`username`) REFERENCES `t_user` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
